import {Injectable} from '@angular/core';
import {Product} from './product.model';


@Injectable()
export class Cart {
  public lines: CartLine[] = [];
  public itemCount = 0;
  public cartPrice = 0;

  addLine(product: Product, quantity: number = 1) {
    const line = this.lines.find(innerLine => innerLine.product.id === product.id);
    if (line !== undefined) {
      line.quantity += quantity;
    } else {
      this.lines.push(new CartLine(product, quantity));
    }
    this.recalculate();
  }

  updateQuantity(product: Product, quantity: number) {
    const line = this.lines.find(innerLine => innerLine.product.id === product.id);
    if (line !== undefined) {
      line.quantity = Number(quantity);
    }
    this.recalculate();
  }

  removeLine(id: number) {
    const self = this;
    self.lines = this.lines.filter(innerLine => innerLine.product.id !== id);
    this.recalculate();
  }

  clear() {
    const self = this;
    self.lines = [];
    self.itemCount = 0;
    self.cartPrice = 0;
  }

  private recalculate() {
    const self = this;
    self.itemCount = 0;
    self.cartPrice = 0;
    self.lines.forEach(line => {
      self.itemCount += line.quantity;
      self.cartPrice += (line.quantity * line.product.price);
    });
  }
}

export class CartLine {
  constructor(public product: Product, public quantity: number) {
  }

  get lineTotal() {
    return this.quantity * this.product.price;
  }
}
